# Mini Project: Breakable Objects
Vector Stenvers & Maurice Manshanden

In this mini project we simulate breakable objects. The breakability of objects can be modeled by two parameters: edge stength and frailty. An object can be made breakable by adding the `Breakable` script. After adding this script, the two parameters become available in the Unity editor.

There are three scenes for demonstration purposes: 
- Collision Scene
- Cylinder Scene
- Glass Scene

# Controls
For all three scenes the controls are the same:
- Holding left click and dragging the mouse will rotate the camera.
- WASD keys will move the camera

# How to open
This project was made using Unity version 2018.11f1, but later versions should also work. To open, clone the repository and open the directory `Mini Project` in Unity. Then open one of the three demo scenes located in the `Scenes` folder.

# Screenshot
![Screenshot of Unity Editor running the Collision Scene](screenshot.png "Screenshot of Unity Editor running the Collision Scene")