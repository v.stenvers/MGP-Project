﻿using UnityEngine;

public class FlyCamera : MonoBehaviour
{
    public float Speed = 10.0f;
    public float Sensitivity = 0.15f;

    private Vector3 MousePosition;

    private void Start()
    {
        MousePosition = Input.mousePosition;
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            this.MousePosition = Input.mousePosition - this.MousePosition;

            Vector3 rotation = new Vector3(
                -this.MousePosition.y * this.Sensitivity,
                this.MousePosition.x * this.Sensitivity,
                0);

            this.transform.eulerAngles = new Vector3(
                this.transform.eulerAngles.x + rotation.x,
                this.transform.eulerAngles.y + rotation.y,
                0);
        }

        this.MousePosition = Input.mousePosition;

        Vector3 velocity = new Vector3();

        if (Input.GetKey(KeyCode.W))
        {
            velocity.z += 1;
        }
        if (Input.GetKey(KeyCode.S))
        {
            velocity.z -= 1;
        }
        if (Input.GetKey(KeyCode.A))
        {
            velocity.x -= 1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            velocity.x += 1;
        }

        velocity = velocity * this.Speed * Time.deltaTime;
        this.transform.Translate(velocity);
    }
}
