﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Basically a wrapper class for the Unity Mesh object, with some 
/// additional functionality that allows the mesh to be altered.
/// </summary>
public class MeshBuilder
{
    public List<Vector3> vertices;
    public List<Vector3> normals;
    public List<Vector2> uv;

    public MeshBuilder(Mesh mesh)
    {
        this.vertices = new List<Vector3>(mesh.vertices);
        this.normals = new List<Vector3>(mesh.normals);
        this.uv = new List<Vector2>(mesh.uv);
    }

    /// <summary>
    /// Builds a <see cref="Mesh"/> object using the provided triangle indices.
    /// </summary>
    /// <param name="triangles">The indices to the triangle vertices.</param>
    /// <returns>A <see cref="Mesh"/> object.</returns>
    public Mesh GetMesh(IList<int> triangles)
    {
        var vertices = new List<Vector3>();
        var normals = new List<Vector3>();
        var uv = new List<Vector2>();

        // Contains for each index in the original triangles array
        // the index for the new triangles array.
        var newIndex = new Dictionary<int, int>();

        // A list of indices to the vertices that make up a triangle.
        // Should always be a multiple of 3.
        var newTriangles = new List<int>();

        foreach (int i in triangles)
        {
            int index;

            if (newIndex.ContainsKey(i))
            {
                // This particular vertex is already inside the mesh
                index = newIndex[i];
            }
            else
            {
                // Add vertex to the new mesh object
                index = newIndex.Count;
                newIndex.Add(i, index);

                vertices.Add(this.vertices[i]);
                normals.Add(this.normals[i]);
                uv.Add(this.uv[i]);
            }

            newTriangles.Add(index);
        }

        var mesh = new Mesh()
        {
            vertices = vertices.ToArray(),
            normals = normals.ToArray(),
            uv = uv.ToArray(),
            triangles = newTriangles.ToArray()
        };

        return mesh;
    }

    /// <summary>
    /// Splits an edge by adding a new vertex on a plane.
    /// </summary>
    /// <param name="index1">Index to the first vertex of the edge.</param>
    /// <param name="index2">Index to the second vertex of the edge.</param>
    /// <param name="plane">Plane along which the edge needs to be split.</param>
    /// <returns>Index to the (new) vertex at the intersection point.</returns>
    public int SplitEdge(int index1, int index2, Plane plane)
    {
        Vector3 v1 = this.vertices[index1];
        Vector3 v2 = this.vertices[index2];

        // Setup a ray along the edge
        Vector3 origin = v1;
        Vector3 direction = v2 - v1;

        // Calculate intersection
        float t = plane.Intersect(origin, direction);

        // Plane does not intersect edge, plane in front
        if (t <= 0)
        {
            return index1;
        }

        // Plane does not intersect edge, plane behind
        if (t >= 1)
        {
            return index2;
        }

        // Calculate intersection point
        Vector3 vertex = origin + direction * t;

        // Interpolate vertex, normal and uv
        Vector3 normal = t * this.normals[index1] + (1 - t) * this.normals[index2];
        Vector2 uv = t * this.uv[index1] + (1 - t) * this.uv[index2];

        this.vertices.Add(vertex);
        this.normals.Add(normal.normalized);
        this.uv.Add(uv);

        return this.vertices.Count - 1;
    }

    /// <summary>
    /// Adds a new vertex to the mesh.
    /// </summary>
    /// <param name="position">Position of the vertex.</param>
    /// <param name="normal">Normal of the vertex.</param>
    /// <param name="uv">Texture coordinate of the vertex.</param>
    /// <returns>Index to the newly generated vertex.</returns>
    public int PushVertex(Vector3 position, Vector3 normal, Vector2 uv)
    {
        this.vertices.Add(position);
        this.normals.Add(normal);
        this.uv.Add(uv);

        return this.vertices.Count - 1;
    }

    /// <summary>
    /// Calculates the center of the vertex positions for the vertices that
    /// belong to the given indices.
    /// </summary>
    /// <param name="indices">Indices of the vertices.</param>
    /// <returns>Center of the vertex positions.</returns>
    public Vector3 GetCenter(IList<int> indices)
    {
        // Get unique indices
        var uniqueIndices = new HashSet<int>(indices);

        // Accumalate all vertex positions of mesh
        var accumalator = new Vector3();
        foreach (int idx in uniqueIndices)
        {
            accumalator += this.vertices[idx];
        }

        // Return average of positions (i.e. center)
        return accumalator * (1.0f / uniqueIndices.Count);
    }

    /// <summary>
    /// Splits a mesh along a plane. Triangles that intersect the plane are
    /// cut in two. The vertices resulting from these cuts are triangulated.
    /// </summary>
    /// <param name="triangles">Triangles defining the mesh.</param>
    /// <param name="plane">The plane defining the cut.</param>
    /// <returns>Indices of the left and right mesh.</returns>
    public (int[] left, int[] right) Split(IList<int> triangles, Plane plane)
    {
        // Indices for left and right triangles
        var left = new List<int>();
        var right = new List<int>();

        // Indices to the newly create vertices when splitting edges
        var edges = new List<int>();

        for (int i = 0; i < triangles.Count; i += 3)
        {
            // Get indices to triangle vertices
            int i1 = triangles[i + 0];
            int i2 = triangles[i + 1];
            int i3 = triangles[i + 2];

            // Get vertices themselves
            Vector3 v1 = this.vertices[i1];
            Vector3 v2 = this.vertices[i2];
            Vector3 v3 = this.vertices[i3];

            // Test for each vertex the side the plane
            bool s1 = plane.SignedDistance(v1) > 0;
            bool s2 = plane.SignedDistance(v2) > 0;
            bool s3 = plane.SignedDistance(v3) > 0;

            if (s1 && s2 && s3)
            {
                left.AddRange(new[] { i1, i2, i3 });
            }
            else if (!s1 && !s2 && !s3)
            {
                right.AddRange(new[] { i1, i2, i3 });
            }
            else
            {
                // The cases where one vertex is on one side, and the other
                // two vertices are on the other side of the plane.

                int s = -1, p1 = -1, p2 = -1;
                bool l = false;

                if (s1 && !s2 && !s3) { s = i1; p1 = i2; p2 = i3; l = true; }
                if (!s1 && s2 && !s3) { s = i2; p1 = i3; p2 = i1; l = true; }
                if (!s1 && !s2 && s3) { s = i3; p1 = i1; p2 = i2; l = true; }

                if (!s1 && s2 && s3) { s = i1; p1 = i2; p2 = i3; l = false; }
                if (s1 && !s2 && s3) { s = i2; p1 = i3; p2 = i1; l = false; }
                if (s1 && s2 && !s3) { s = i3; p1 = i1; p2 = i2; l = false; }

                int x1 = this.SplitEdge(s, p1, plane);
                int x2 = this.SplitEdge(s, p2, plane);

                if (l)
                {
                    left.AddRange(new[] { x1, x2, s });
                    right.AddRange(new[] { x1, p1, p2, p2, x2, x1 });
                }
                else
                {
                    left.AddRange(new[] { x1, p1, p2, p2, x2, x1 });
                    right.AddRange(new[] { x1, x2, s });
                }

                edges.Add(this.PushVertex(this.vertices[x1], plane.Normal, this.uv[x1]));
                edges.Add(this.PushVertex(this.vertices[x2], plane.Normal, this.uv[x2]));
            }
        }

        // New vertex at center of splitting plane
        int a = this.PushVertex(plane.Origin, plane.Normal, Vector2.zero);

        // Triangulate the new vertices from center of splitting plane (only works for convex shapes)
        for (int i = 0; i < edges.Count; i += 2)
        {
            int b = edges[i];
            int c = edges[i + 1];

            left.AddRange(new[] { a, b, c, c, b, a });
            right.AddRange(new[] { a, b, c, c, b, a });
        }

        return (left.ToArray(), right.ToArray());
    }
}
