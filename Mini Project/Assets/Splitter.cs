﻿using System.Collections.Generic;
using UnityEngine;

public class MeshTreeNode
{
    public MeshTreeNode left;
    public MeshTreeNode right;
    public HashSet<int> leaves;
    public Mesh mesh;
}

public class Splitter : MonoBehaviour
{
    public MeshTreeNode root;
    public List<GraphEdge> graph;
    public List<MeshTreeNode> leaves;

    float time = 0;
    MeshTreeNode current;

    public int seed;
    public float edgeStrength;

    void Start()
    {
        Random.InitState(this.seed);

        this.BuildMeshTree();
        this.BuildConnectivityGraph();

        this.GetComponent<MeshFilter>().mesh = this.root.mesh;
        this.current = this.root;
    }

    void BuildMeshTree()
    {
        this.root = new MeshTreeNode
        {
            mesh = this.GetComponent<MeshFilter>().mesh,
        };

        this.SplitNode(this.root);
    }

    // Compare fragment bounding boxes to create a connectivity graph
    void BuildConnectivityGraph()
    {
        this.graph = new List<GraphEdge>();

        // Collect all fragments in a list
        this.leaves = new List<MeshTreeNode>();
        this.GetLeaves(this.root, ref this.leaves);

        // Iterate over all leaves/fragments, building a connectivity graph
        for (int i = 0; i < this.leaves.Count; i++)
        {
            MeshTreeNode leafA = this.leaves[i];
            Mesh A = leafA.mesh;

            for (int j = i + 1; j < this.leaves.Count; j++)
            {
                MeshTreeNode leafB = this.leaves[j];
                Mesh B = leafB.mesh;

                // Check if the two AABBs intersect
                if (A.bounds.Intersects(B.bounds))
                {
                    Vector3 midpoint = B.bounds.center - A.bounds.center;

                    // Create an edge between the two fragments
                    GraphEdge edge = new GraphEdge
                    {
                        A = i,
                        B = j,
                        Strength = edgeStrength,
                        Midpoint = midpoint,
                        Broken = false
                    };

                    this.graph.Add(edge);
                }
            }
        }

    }

    // Gather all leaves from a given MeshTree recursively (depth first)
    bool GetLeaves(MeshTreeNode node, ref List<MeshTreeNode> leaves)
    {
        node.leaves = new HashSet<int>();

        if (node.left != null)
        {
            bool leaf = this.GetLeaves(node.left, ref leaves);

            // If the node is a leaf, add it to the leaves HashSet
            if (leaf)
            {
                node.leaves.Add(leaves.Count - 1);
            }
            // Else, add all leaves of the child to this node's leaves Hashset
            else
            {
                node.leaves.UnionWith(node.left.leaves);
            }
        }

        if (node.right != null)
        {
            bool leaf = this.GetLeaves(node.right, ref leaves);

            // If the node is a leaf, add it to the leaves HashSet
            if (leaf)
            {
                node.leaves.Add(leaves.Count - 1);
            }
            // Else, add all leaves of the child to this node's leaves Hashset
            else
            {
                node.leaves.UnionWith(node.right.leaves);
            }
        }

        // If this node does not have any children, it is a leaf
        if (node.left == null && node.right == null)
        {
            leaves.Add(node);
            return true;
        }

        return false;
    }

    private void Update()
    {
        this.time += Time.deltaTime;

        if (this.time > 0.3 && (Input.GetKey(KeyCode.N) || Input.GetKey(KeyCode.M)))
        {
            this.time = 0;

            if (Input.GetKey(KeyCode.N))
            {
                this.current = this.current.left;
            }
            else
            {
                this.current = this.current.right;
            }

            if (this.current == null)
            {
                this.current = this.root;
            }

            this.GetComponent<MeshFilter>().mesh = this.current.mesh;

            this.GetComponent<MeshCollider>().sharedMesh = this.current.mesh;
        }
    }

    void SplitNode(MeshTreeNode node, int depth = 6)
    {
        if (depth <= 0)
        {
            return;
        }

        MeshBuilder builder = new MeshBuilder(node.mesh);

        // Splitting plane
        Plane plane = new Plane(node.mesh.bounds.center, Random.onUnitSphere);

        (int[] left, int[] right) = builder.Split(node.mesh.triangles, plane);

        node.left = new MeshTreeNode()
        {
            mesh = builder.GetMesh(left)
        };

        node.right = new MeshTreeNode()
        {
            mesh = builder.GetMesh(right)
        };

        this.SplitNode(node.left, depth - 1);
        this.SplitNode(node.right, depth - 1);
    }
}
