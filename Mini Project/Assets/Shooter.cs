﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public GameObject Projectile;
    public float Force = 100.0f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Projectile.transform.position = transform.position + transform.forward * 2;
            Projectile.GetComponent<Rigidbody>().velocity = transform.forward * Force;
        }
    }
}
