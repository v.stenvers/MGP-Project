﻿using UnityEngine;

public class Plane
{
    public Vector3 Origin { get; }
    public Vector3 Normal { get; }

    private readonly float a;
    private readonly float b;
    private readonly float c;
    private readonly float d;

    private readonly float denom;

    public Plane(Vector3 origin, Vector3 normal)
    {
        this.a = normal.x;
        this.b = normal.y;
        this.c = normal.z;
        this.d = -this.a * origin.x - this.b * origin.y - this.c * origin.z;

        this.Origin = origin;
        this.Normal = normal;

        this.denom = 1.0f / Mathf.Sqrt(this.a * this.a + this.b * this.b + this.c * this.c);
    }

    public Plane(Vector3 p1, Vector3 p2, Vector3 p3)
        : this(p1, Vector3.Cross(p2 - p1, p3 - p1))
    { }

    public float SignedDistance(Vector3 point)
    {
        return (this.a * point.x + this.b * point.y + this.c * point.z + this.d) * this.denom;
    }

    public float Intersect(Vector3 origin, Vector3 direction)
    {
        return Vector3.Dot(this.Origin - origin, this.Normal) / Vector3.Dot(direction, this.Normal);
    }
}
