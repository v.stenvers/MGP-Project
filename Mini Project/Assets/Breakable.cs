﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// This script can be added to game objects in the Unity editor and will prepare the 
/// game object for simulation by splitting the mesh and creating BreakableInstance
/// version.
/// </summary>
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class Breakable : BreakableInstance
{
    [Tooltip("Seed that is used for randomly generating splitting planes.")]
    public int RandomSeed = 0;

    [Tooltip("The number of recursive splits that will be made to subdivide the mesh into fragments.")]
    public int NumberOfSplits = 6;

    [Tooltip("The initial strenth of the edges that hold the fragments together.")]
    public float InitialEdgeStrength = 4.0f;

    [Tooltip("Determines how prone the object is to breaking.")]
    public float Frailty = 1.0f;

    // Start is called before the first frame update
    private void Start()
    {
        Random.InitState(this.RandomSeed);

        // Create the root node of the mesh tree
        var root = new MeshTreeNode()
        {
            mesh = this.GetComponent<MeshFilter>().mesh
        };

        // Split the mesh tree into fragments
        this.SplitNodeRecursively(root, this.NumberOfSplits);
        this.AddLeavesToNodes(root);

        // Get the actual fragments from the tree
        List<MeshTreeNode> fragments = this.GetLeaves(root);
        fragments.TrimExcess();

        // Get the graph with edges between overlapping fragments
        Graph<FragmentEdge> graph = this.InitialGraph(fragments);

        var pieces = new List<int>();
        for (int i = 0; i < fragments.Count; i++)
        {
            pieces.Add(i);
        }

        // Spawn a new gameobject that will be used for simulation
        this.SpawnPiece(new InitializationArguments()
        {
            root = root,
            fragments = fragments,
            graph = graph,
            pieces = pieces,
            material = this.GetComponent<MeshRenderer>().material,
            frailty = Frailty
        }, this);

        // The spawned piece replaces this object, so this object is no longer needed
        Destroy(this.gameObject);
    }

    private void AddLeavesToNodes(MeshTreeNode node)
    {
        var leaves = new List<MeshTreeNode>();
        var stack = new Stack<MeshTreeNode>();
        stack.Push(node);

        while (stack.Count > 0)
        {
            MeshTreeNode n = stack.Pop();
            n.leaves = new HashSet<MeshTreeNode>(GetLeaves(n));

            if (n.left != null)
            {
                stack.Push(n.left);
            }

            if (n.right != null)
            {
                stack.Push(n.right);
            }

            if (n.left == null && n.right == null)
            {
                leaves.Add(n);
            }
        }
    }

    /// <summary>
    /// Splits the mesh associated to the <see cref="MeshTreeNode"/> into a left
    /// and right part by a random plane through the center of the mesh.
    /// </summary>
    /// <param name="node">The node containing the mesh that needs to be split.</param>
    /// <param name="splits">The number of times the node needs to be split recursively.</param>
    private void SplitNodeRecursively(MeshTreeNode node, int splits)
    {
        if (splits <= 0)
        {
            return;
        }

        var builder = new MeshBuilder(node.mesh);

        // Construct random splitting plane through mesh center
        var plane = new Plane(node.mesh.bounds.center, Random.onUnitSphere);

        // Split the mesh into two parts using the random plane
        (int[] left, int[] right) = builder.Split(node.mesh.triangles, plane);

        node.left = new MeshTreeNode()
        {
            mesh = builder.GetMesh(left)
        };

        node.right = new MeshTreeNode()
        {
            mesh = builder.GetMesh(right)
        };

        // Recurse...
        this.SplitNodeRecursively(node.left, splits - 1);
        this.SplitNodeRecursively(node.right, splits - 1);
    }

    /// <summary>
    /// Creates a graph with edges between fragments with overlapping bounding boxes.
    /// </summary>
    /// <param name="fragments">The nodes containing the meshes that represent the fragments.</param>
    /// <returns>A list of edges between fragments with overlapping bounding boxes.</returns>
    private Graph<FragmentEdge> InitialGraph(IList<MeshTreeNode> fragments)
    {
        var graph = new Graph<FragmentEdge>(fragments.Count);

        // Loop over every pair of fragments
        for (int i = 0; i < fragments.Count; i++)
        {
            Mesh a = fragments[i].mesh;

            for (int j = i + 1; j < fragments.Count; j++)
            {
                Mesh b = fragments[j].mesh;

                if (a.bounds.Intersects(b.bounds))
                {
                    // Calculate the midpoint between the two fragments, this will
                    // represent the "location" of the edge.
                    Vector3 mid = a.bounds.center * 0.5f + b.bounds.center * 0.5f;

                    graph.Connect(i, j, new FragmentEdge()
                    {
                        midpoint = mid - transform.position,
                        strength = InitialEdgeStrength,
                        broken = false
                    });
                }
            }
        }

        return graph;
    }
}

public class BreakableInstance : MonoBehaviour
{
    protected class MeshTreeNode
    {
        public MeshTreeNode left;
        public MeshTreeNode right;
        public Mesh mesh;
        public HashSet<MeshTreeNode> leaves;
    }

    protected class FragmentEdge
    {
        public float strength;
        public Vector3 midpoint;
        public bool broken;
    }

    protected class InitializationArguments
    {
        public MeshTreeNode root;
        public List<MeshTreeNode> fragments;
        public Graph<FragmentEdge> graph;
        public List<int> pieces;

        public Material material;
        public float frailty;
    }

    private MeshTreeNode root;
    private List<MeshTreeNode> fragments;
    private Graph<FragmentEdge> graph;
    private List<int> pieces;

    private Material material;
    private float frailty;

    private void Initialize(InitializationArguments arguments)
    {
        this.root = arguments.root;
        this.fragments = arguments.fragments;
        this.graph = arguments.graph;
        this.pieces = arguments.pieces;

        this.material = arguments.material;
        this.frailty = arguments.frailty;

        // Add submeshes to create a compound collider
        List<Mesh> meshes = this.FindMinimalMeshes();

        foreach (Mesh mesh in meshes)
        {
            var sub = new GameObject("Submesh");
            sub.transform.parent = this.transform;
            sub.transform.position = this.transform.position;

            MeshFilter meshf = sub.AddComponent<MeshFilter>();
            MeshCollider meshc = sub.AddComponent<MeshCollider>();
            MeshRenderer meshr = sub.AddComponent<MeshRenderer>();

            meshf.mesh = meshc.sharedMesh = mesh;
            meshr.material = this.material;
            meshc.convex = true;
        }
    }

    private List<Mesh> FindMinimalMeshes()
    {
        var body = new HashSet<MeshTreeNode>(this.pieces.Select(i => this.fragments[i]));
        var meshes = new List<Mesh>();

        var queue = new Queue<MeshTreeNode>();
        queue.Enqueue(this.root);

        while (queue.Count > 0)
        {
            MeshTreeNode n = queue.Dequeue();
            
            if (n.leaves.IsSubsetOf(body))
            {
                meshes.Add(n.mesh);
            }
            else
            {
                if (n.left != null)
                {
                    queue.Enqueue(n.left);
                }

                if (n.right != null)
                {
                    queue.Enqueue(n.right);
                }
            }
        }

        return meshes;
    }

    /// <summary>
    /// Returns a list containing the leaves of the (sub)tree.
    /// </summary>
    /// <param name="node">A node in the tree.</param>
    /// <returns>A leaves belonging to the given node.</returns>
    protected List<MeshTreeNode> GetLeaves(MeshTreeNode node)
    {
        var leaves = new List<MeshTreeNode>();
        var stack = new Stack<MeshTreeNode>();
        stack.Push(node);

        while (stack.Count > 0)
        {
            MeshTreeNode n = stack.Pop();

            if (n.left != null)
            {
                stack.Push(n.left);
            }

            if (n.right != null)
            {
                stack.Push(n.right);
            }

            if (n.left == null && n.right == null)
            {
                leaves.Add(n);
            }
        }

        return leaves;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (this.pieces.Count == 1)
        {
            // Fragments cannot break further
            return;
        }

        float damage = this.frailty * collision.impulse.magnitude;
        Vector3 contact = collision.GetContact(0).point - transform.position;

        int count = 0;

        // Break edges that could not handle the damage.
        for (int i = 0; i < this.graph.Edges.Count; i++)
        {
            float dist = (this.graph.Edges[i].midpoint - contact).magnitude;
            float inv_dist = 1.0f / Mathf.Pow(dist, 3);

            if (damage * inv_dist > this.graph.Edges[i].strength)
            {
                this.graph.Edges[i].broken = true;
                count++;
            }
        }

        if (count > 0)
        {
            Debug.Log($"{count} edges broken (this includes already broken edges)");
        }

        List<List<int>> components = this.FindConnectedComponents();

        if (components.Count < 2)
        {
            return;
        }

        Debug.Log($"{components.Count} new bodies");

        foreach (List<int> component in components)
        {
            this.SpawnPiece(new InitializationArguments()
            {
                root = this.root,
                fragments = this.fragments,
                graph = this.graph,
                pieces = component,
                material = this.material,
                frailty = this.frailty
            }, this);
        }

        Destroy(this.gameObject);
    }

    private List<List<int>> FindConnectedComponents()
    {
        var remaining = new List<int>(this.pieces);
        var result = new List<List<int>>();

        while (remaining.Count > 0)
        {
            List<int> component = this.graph.BreadthFirstSearch(remaining[0], e => e.broken == false);

            result.Add(component);
            
            foreach (int c in component)
            {
                remaining.Remove(c);
            }

        }

        return result;
    }

    protected void SpawnPiece(InitializationArguments arguments, BreakableInstance previous)
    {
        // The parent gameobject.
        var piece = new GameObject("Breakable Piece");
        piece.transform.parent = previous.transform.parent;
        piece.transform.position = previous.transform.position;
        piece.transform.rotation = previous.transform.rotation;

        Rigidbody rigidbody = piece.AddComponent<Rigidbody>();

        // Add breakable 
        BreakableInstance instance = piece.AddComponent<BreakableInstance>();
        instance.Initialize(arguments);
    }
}
