﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphEdge
{
    public int A, B;
    public bool Broken;
    public float Strength;
    public Vector3 Midpoint;
}

public class CollisionHandler : MonoBehaviour
{
    private List<GraphEdge> graph;
    private Splitter splitter;

    public float Frailty;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Called on collision
    void OnCollisionEnter(Collision collision)
    {
        splitter = gameObject.GetComponent(typeof(Splitter)) as Splitter;
        graph = splitter.graph;

        BreakEdges(collision);
        List<int>[] meshTrees = RecomputeConnectivity();

        for (int i = 0; i < meshTrees.Length; i++)
        {
            GetMinimalMesh(meshTrees[i]);
        }
        
    }

    void BreakEdges(Collision collision)
    {
        float damage = Frailty * collision.impulse.magnitude;

        // Check for each edge if it is broken
        Vector3 contactPoint = collision.GetContact(0).point;
        for (int i = 0; i < graph.Count; i++)
        {
            GraphEdge edge = graph[i];
            if (edge.Broken)
                continue;

            float dist = (edge.Midpoint - contactPoint).magnitude;
            float inv_dist = 1.0f / (float)Math.Pow(dist, 3);

            // Remove the edge if the damage exceeds the edge strength
            if (damage * inv_dist > edge.Strength)
            {
                graph[i].Broken = true;
            }
        }
    }

    List<int>[] RecomputeConnectivity()
    {
        // Create a list for each node containing the node
        List<int>[] nodeLists = new List<int>[splitter.leaves.Count];
        for (int i = 0; i < nodeLists.Length; i++)
        {
            nodeLists[i] = new List<int>();
            nodeLists[i].Add(i);
        }

        // Iterate over all edges in the graph
        bool[] merged = new bool[nodeLists.Length];
        for (int i = 0; i < graph.Count; i++)
        {
            GraphEdge edge = graph[i];
            if (edge.Broken)
                continue;

            // Merge the two nodeLists and mark the second vertex of the edge as merged
            nodeLists[edge.A].AddRange(nodeLists[edge.B]);
            merged[edge.B] = true;
        }

        // Only return unique graphs
        List<List<int>> result = new List<List<int>>();
        for (int i = 0; i < nodeLists.Length; i++)
        {
            if (!merged[i])
                result.Add(nodeLists[i]);
        }

        return result.ToArray();
    }

    MeshTreeNode GetMinimalMesh(List<int> meshTree)
    {
        bool found = false;
        MeshTreeNode result = null;

        // Search through the original mesh tree (BFS)
        Queue<MeshTreeNode> queue = new Queue<MeshTreeNode>();
        queue.Enqueue(splitter.root);
        while (queue.Count > 0)
        {
            MeshTreeNode node = queue.Dequeue();

            // If all nodes of a graph are contained in the node
            bool validNode = node.leaves.IsSupersetOf(meshTree);
            if (validNode)
            {
                // Add the node to the result
                result = node;

                // Mark the contained meshtree as found
                found = true;
            }
            // Else, if we have already found a valid node
            else if (found)
            {
                // Do not continue to traverse this part of the mesh tree
                continue;
            }

            // Traverse the tree to find a smaller, fitting mesh
            if (node.left != null)
            {
                queue.Enqueue(node.left);
            }
            if (node.right != null)
            {
                queue.Enqueue(node.right);
            }
        }

        return result;
    }
}
