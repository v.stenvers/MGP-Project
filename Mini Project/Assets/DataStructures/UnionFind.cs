﻿public class UnionFind
{
    private readonly int[] parent;
    private readonly int[] rank;

    public UnionFind(int count)
    {
        this.parent = new int[count];

        for (int i = 0; i < count; i++)
        {
            this.parent[i] = -1;
        }

        this.rank = new int[count];
    }

    public void MakeSet(int value)
    {
        this.parent[value] = value;
    }
    
    public int Find(int value)
    {
        if (this.parent[value] != value)
        {
            this.parent[value] = this.Find(this.parent[value]);
        }

        return this.parent[value];
    }

    public void Union(int a, int b)
    {
        int x = this.Find(a);
        int y = this.Find(b);

        if (x == y)
        {
            return;
        }

        if (this.rank[x] < this.rank[y])
        {
            int t = x;
            x = y;
            y = t;
        }

        this.parent[y] = x;

        if (this.rank[x] == this.rank[y])
        {
            this.rank[x] = this.rank[y] + 1;
        }
    }
}
