﻿using System;
using System.Collections.Generic;

public class Graph<T>
{
    struct Edge
    {
        public int from;
        public int to;
        public T satellite;
    }

    private readonly LinkedList<Edge>[] adjacencyLists;
    private readonly List<T> edges;

    public IReadOnlyList<T> Edges;    

    public Graph(int nodes)
    {
        this.adjacencyLists = new LinkedList<Edge>[nodes];
        this.edges = new List<T>();

        for (int i = 0; i < nodes; i++)
        {
            adjacencyLists[i] = new LinkedList<Edge>();
        }
    }

    public void Connect(int from, int to, T connection)
    {
        var edgeA = new Edge
        {
            from = from,
            to = to,
            satellite = connection
        };

        var edgeB = new Edge
        {
            from = to,
            to = from,
            satellite = connection
        };

        this.adjacencyLists[from].AddLast(edgeA);
        this.adjacencyLists[to].AddLast(edgeB);

        this.edges.Add(connection);
        this.Edges = this.edges.AsReadOnly();
    }

    public List<int> BreadthFirstSearch(int start, Func<T, bool> condition)
    {
        var visited = new HashSet<int>();
        var nodes = new List<int>();

        var queue = new Queue<int>();
        queue.Enqueue(start);

        while (queue.Count > 0)
        {
            int n = queue.Dequeue();

            if (visited.Contains(n))
            {
                continue;
            }

            visited.Add(n);
            nodes.Add(n);

            foreach (Edge adjacent in this.adjacencyLists[n])
            {
                if (condition(adjacent.satellite))
                {
                    queue.Enqueue(adjacent.to);
                }
            }
        }

        return nodes;
    }
}